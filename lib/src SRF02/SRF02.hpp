#pragma once


typedef unsigned char uint8;
class SRF02 {

public:
    SRF02(uint8 address, uint8 sda ,uint8 scl);         //Constructeur
    ~SRF02();               //Destructeur
    int LireDistance();             //fonction pour la distance

private:
    int reading;
    int sda;
    int scl;
    int addressI2C;
};